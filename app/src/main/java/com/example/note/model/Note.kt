package com.example.note.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "note_table")
data class Note(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val date: String,
    val title: String,
    val details: String,
    val survey: ArrayList<String>
)
