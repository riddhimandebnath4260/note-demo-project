package com.example.note.data.repo

import androidx.lifecycle.LiveData
import com.example.note.model.Note

class NoteRepository(private val noteDao: NoteDao) {

    val readNoteData: LiveData<List<Note>> = noteDao.readAllNote()

    suspend fun addNoteData(note: Note){
        noteDao.addNote(note)
    }
}