package com.example.note.data.source.local

import android.content.Context
import androidx.room.*
import com.example.helper.DateTypeConverter
import com.example.helper.ListTypeConverter
import com.example.note.data.repo.NoteDao
import com.example.note.model.Note

@Database(entities = [Note::class], version = 3, exportSchema = false)
@TypeConverters(DateTypeConverter::class,ListTypeConverter::class)
abstract class NoteDataBase: RoomDatabase() {

    abstract fun noteDao(): NoteDao

    companion object{
        @Volatile
        private var INSTANCE: NoteDataBase?=null

        fun getDatabase(context: Context): NoteDataBase {
            val tempInstance = INSTANCE
            if (tempInstance!=null){
                return tempInstance
            }
            synchronized(this){
                val dataBase = Room.databaseBuilder(context.applicationContext,
                NoteDataBase::class.java,"Note-DataBase").build()
                INSTANCE = dataBase
                return dataBase
            }
        }
    }
}