package com.example.note.ui

import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.note.databinding.ActivityNoteAddBinding
import com.example.note.model.Note
import com.google.android.material.datepicker.MaterialDatePicker

class NoteAddActivity : AppCompatActivity() {

    private lateinit var binding: ActivityNoteAddBinding
    private lateinit var mNoteViewModel: NoteViewModel


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityNoteAddBinding.inflate(layoutInflater)
        setContentView(binding.root)

        mNoteViewModel = ViewModelProvider(this).get(NoteViewModel::class.java)

        val materialDatePicker = MaterialDatePicker.Builder.datePicker().setTitleText("Select Date")
            .setSelection(MaterialDatePicker.todayInUtcMilliseconds()).build()

        binding.noteAddDate.setOnClickListener{
            materialDatePicker.show(supportFragmentManager,"Date Picker")
            materialDatePicker.addOnPositiveButtonClickListener{
                binding.noteAddDate.text = materialDatePicker.headerText
            }

        }
        binding.addNoteBtn.setOnClickListener {
            insertData()
        }


    }


    private fun insertData() {

        val noteSurveyList = ArrayList<String>()

        val booleanCheckFirst = binding.firstSurvey.isChecked

        if (booleanCheckFirst){
            noteSurveyList.add("1")
        }
        else{
            noteSurveyList.add("0")
        }

        val booleanCheckSecond = binding.secondSurvey.isChecked

        if (booleanCheckSecond){
            noteSurveyList.add("1")
        }
        else{
            noteSurveyList.add("0")
        }
        Log.d("test1",noteSurveyList.toString())


        val noteTitle = binding.nodeAddTitleEt.text.toString()
        val noteDetail = binding.nodeAddDetailEt.text.toString()
        val noteDate = binding.noteAddDate.text.toString()

        if (inputCheck(noteTitle,noteDetail)){
            val note = Note(0,noteDate,noteTitle,noteDetail,noteSurveyList)
            mNoteViewModel.addNoteViewModel(note)
            Toast.makeText(this,"Successfully Added",Toast.LENGTH_LONG).show()
        }
        else{
            Toast.makeText(this,"Please Fill All Scope",Toast.LENGTH_LONG).show()
        }
    }

    private fun inputCheck(title: String,detail:String): Boolean {
        return !(TextUtils.isEmpty(title) || TextUtils.isEmpty(detail))
    }
}