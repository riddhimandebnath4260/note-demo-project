package com.example.note.ui

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.note.data.source.local.NoteDataBase
import com.example.note.data.repo.NoteRepository
import com.example.note.model.Note
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class NoteViewModel(application: Application):AndroidViewModel(application) {

    val readDtViewModel: LiveData<List<Note>>
    private val noteRepository: NoteRepository

    init {
        val noteDaoViewModel = NoteDataBase.getDatabase(application).noteDao()
        noteRepository = NoteRepository(noteDaoViewModel)
        readDtViewModel = noteRepository.readNoteData
    }

    fun addNoteViewModel(note: Note){
        viewModelScope.launch(Dispatchers.IO) {
            noteRepository.addNoteData(note)
        }
    }
}