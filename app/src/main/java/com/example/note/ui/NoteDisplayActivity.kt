package com.example.note.ui

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.note.databinding.ActivityNotedisplayBinding
import com.example.note.ui.adapter.NoteAdapter

class NoteDisplayActivity : AppCompatActivity() {

    private lateinit var binding: ActivityNotedisplayBinding
    private lateinit var noteViewModel: NoteViewModel
    private lateinit var noteDisplayAdapter: NoteAdapter



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityNotedisplayBinding.inflate(layoutInflater)
        setContentView(binding.root)

        noteDisplayAdapter = NoteAdapter()
        binding.notesList.layoutManager = LinearLayoutManager(this)
        binding.notesList.adapter = noteDisplayAdapter

        noteViewModel = ViewModelProvider(this).get(NoteViewModel::class.java)
        noteViewModel.readDtViewModel.observe(this, Observer {
            noteDisplayAdapter.submitList(it)
        })

        binding.addBtn.setOnClickListener {
            startActivity(Intent(this,NoteAddActivity::class.java))
        }



    }

}