package com.example.note.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.note.databinding.ItemNotesBinding
import com.example.note.model.Note

class NoteAdapter : Adapter<ViewHolder>() {

    private val mDiffer = AsyncListDiffer(this,NoteDiffUtil())
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemNotesBinding = ItemNotesBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return NoteViewHolder(itemNotesBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mDiffer.currentList[position]
        (holder as NoteViewHolder).bind(item)
    }

    override fun getItemCount(): Int {
        return mDiffer.currentList.size
    }

    fun submitList(list: List<Note>){
        mDiffer.submitList(list)
    }


    inner class NoteViewHolder(private val itemNotesBinding: ItemNotesBinding) : RecyclerView.ViewHolder(itemNotesBinding.root) {

        fun bind(note: Note){
            itemNotesBinding.notesTitle.text = note.title
            itemNotesBinding.notesDetail.text = note.details
            itemNotesBinding.notesDate.text = note.date.toString()
        }

    }

}
